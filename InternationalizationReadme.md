The package internationalization demos the internationalization in spring

We have hello method in HelloController which will return message "Good morning" based onlocale

Configuration needed:
AcceptHeaderLocaleResovler needs to be setup: it will read the locale value form the Accept-Language header automatically

spring.messages.basename property needs to be setup in the application.properties. 
This property tells spring the basename of the properties files that contain
the locale specific data.
eg: default locale file is messages.properties
for fr locale the filename will be messages_fr.properties

the properties files are placed in the resources folder