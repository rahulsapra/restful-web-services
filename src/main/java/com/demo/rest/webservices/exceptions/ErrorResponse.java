package com.demo.rest.webservices.exceptions;

import lombok.Data;

import java.util.Date;

@Data
public class ErrorResponse {

    private Date timeStamp;
    private String message;
    private String details;

    public ErrorResponse(Date timeStamp, String message, String details) {
        this.timeStamp = timeStamp;
        this.message = message;
        this.details = details;
    }
}
