package com.demo.rest.webservices.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@Data
@ApiModel(description = "a post entity")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int postId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private User user;

    @ApiModelProperty(notes = "date on which the post was created")
    private Date date;

    @ApiModelProperty(notes = "the content of the post")
    private String details;

    public Post(int postId, Date date, String details) {
        this.postId = postId;
        this.date = date;
        this.details = details;
    }
}
