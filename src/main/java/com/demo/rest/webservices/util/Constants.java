package com.demo.rest.webservices.util;

public class Constants {

    public static final String USER_NOT_FOUND = "user not found";
    public static final String POST_NOT_FOUND = "post not found";
}
