package com.demo.rest.webservices.controller;

import com.demo.rest.webservices.exceptions.PostNotFoundException;
import com.demo.rest.webservices.exceptions.UserNotFoundException;
import com.demo.rest.webservices.model.Post;
import com.demo.rest.webservices.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PostController {

    @Autowired
    private PostService postService;

    @GetMapping(path = "/users/{userId}/posts")
    public List<Post> getPosts(@PathVariable("userId") int userId) throws UserNotFoundException {
        return postService.getPosts(userId);
    }

    @GetMapping(path = "/users/{userId}/posts/{postId}")
    public Post getPost(@PathVariable("userId") int userId, @PathVariable("postId") int postId) throws UserNotFoundException, PostNotFoundException {
        return postService.getPost(userId, postId);
    }

    @PostMapping(path = "/users/{userId}/posts")
    public void savePost(@PathVariable("userId") int userId, @Valid @RequestBody Post post) throws UserNotFoundException {
        postService.save(userId, post);
    }

    @DeleteMapping("/users/{userId}/posts/{postId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePost(@PathVariable("userId") int userId, @PathVariable("postId") int postId) throws UserNotFoundException, PostNotFoundException {
        postService.deletePost(userId, postId);
    }
}
