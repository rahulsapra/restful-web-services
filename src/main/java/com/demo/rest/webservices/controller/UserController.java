package com.demo.rest.webservices.controller;

import com.demo.rest.webservices.exceptions.UserNotFoundException;
import com.demo.rest.webservices.model.User;
import com.demo.rest.webservices.requests.SaveUserRequest;
import com.demo.rest.webservices.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.net.URI;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "/users")
@Validated //required to validate the path and query params
public class UserController {

    @Autowired
    private UserService userService;

    private static Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @GetMapping
    public List<User> getUsers() {

        List<User> users = userService.getUsers();
        return users;
    }

    @GetMapping(path = "/{id}")
    public User getUser(@Valid @Min(1) @PathVariable(name = "id") int id) throws UserNotFoundException {
        return userService.getUser(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> saveUser(@Valid @RequestBody SaveUserRequest user) {
        User userRequest = User.builder().name(user.getName()).birthDate(user.getBirthDate()).build();
        User createdUser = userService.saveUser(userRequest);

        //getting the url for the newly created user
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").
                buildAndExpand(createdUser.getId()).toUri();

        return ResponseEntity.created(uri).build();


    }

    @PatchMapping("/{userId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateUser(@PathVariable("userId") int userId, @RequestBody User user) throws UserNotFoundException {
        User user1 = userService.getUser(userId);
        if(user.getName() != null){
            user1.setName(user.getName());
        }
        userService.saveUser(user1);
    }

    @DeleteMapping("/{userId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable("userId") int userId) {
        LOGGER.info("---------------");
        try{
            userService.delete(userId);
            LOGGER.info("deleted user successfully");
        }
        catch (Exception ex) {
            LOGGER.info("error while deleting user: {} with message {} ", userId, ex.getMessage());
        }

    }
}
