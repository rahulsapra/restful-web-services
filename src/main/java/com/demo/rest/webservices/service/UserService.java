package com.demo.rest.webservices.service;

import com.demo.rest.webservices.exceptions.UserNotFoundException;
import com.demo.rest.webservices.model.User;
import com.demo.rest.webservices.repositories.UserRepository;
import com.demo.rest.webservices.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getUsers() {
        List<User> users = (List<User>) userRepository.findAll();
        return users;
    }

    public User getUser(int id) throws UserNotFoundException {

        Optional<User> optionalUser = userRepository.findById(id);

        if (!optionalUser.isPresent()) {
            throw new UserNotFoundException(Constants.USER_NOT_FOUND);
        } else {
            return optionalUser.get();
        }
    }

    public User saveUser(User user) {

        User createdUser = userRepository.save(user);
        return createdUser;
    }

    public void delete(int userId) {

        userRepository.deleteById(userId);
    }
}
