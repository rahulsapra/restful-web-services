package com.demo.rest.webservices.service;

import com.demo.rest.webservices.exceptions.PostNotFoundException;
import com.demo.rest.webservices.exceptions.UserNotFoundException;
import com.demo.rest.webservices.model.Post;
import com.demo.rest.webservices.model.User;
import com.demo.rest.webservices.repositories.PostRepository;
import com.demo.rest.webservices.repositories.UserRepository;
import com.demo.rest.webservices.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PostService {


    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;
    private UserNotFoundException userNotFoundException;

    public List<Post> getPosts(int userId) throws UserNotFoundException {
        Optional<User> optionalUser = userRepository.findById(userId);

        if (!optionalUser.isPresent()) {
            throw new UserNotFoundException(Constants.USER_NOT_FOUND);
        } else {
            return optionalUser.get().getPosts();
        }

    }

    public Post getPost(int userId, int postId) throws UserNotFoundException, PostNotFoundException {
        List<Post> posts = getPosts(userId);

        for (Post post : posts) {
            if (post.getPostId() == postId) {
                return post;
            }
        }
        throw new PostNotFoundException(Constants.POST_NOT_FOUND);
    }


    public void save(int userId, Post post) throws UserNotFoundException {
        Optional<User> optionalUser = userRepository.findById(userId);
        if (!optionalUser.isPresent()) {
            throw new UserNotFoundException(Constants.USER_NOT_FOUND);
        }

        User user = optionalUser.get();
        post.setUser(user);

        postRepository.save(post);
    }

    public void deletePost(int userId, int postId) throws UserNotFoundException, PostNotFoundException {
        List<Post> posts = getPosts(userId);

        boolean isPostCreatedByUser = false;
        for (Post post : posts) {
            if (post.getPostId() == postId) {
                isPostCreatedByUser = true;
            }
        }

        if (isPostCreatedByUser) {
            postRepository.deleteById(postId);
        } else {
            throw new PostNotFoundException(Constants.POST_NOT_FOUND);
        }
    }
}
