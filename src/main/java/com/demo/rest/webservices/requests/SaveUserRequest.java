package com.demo.rest.webservices.requests;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@NoArgsConstructor
@ApiModel("save user request")
public class SaveUserRequest {

    @NotNull
    @Size(min = 4, message = "name should be atleast 4 characters long")
    @ApiModelProperty(notes = "name should be atleast 4 characters long")
    private String name;

    @Past(message = "date should be in past")
    @ApiModelProperty(notes =  "birthDate should be in the past")
    private Date birthDate;
}
